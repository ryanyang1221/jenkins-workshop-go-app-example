library 'jenkins-workshop-jenkins-library@master'

env.AWS_DEFAULT_REGION = 'ap-northeast-2'
env.SERVICE_NAME = 'jenkins-workshop-go-app'
env.ECR_IMAGE_TAG = null
env.PACKAGE_VERSION = '0.0.1'

properties([[$class: 'GitLabConnectionProperty', gitLabConnection: 'gitlab-connection']])

podTemplate(serviceAccount: 'jenkins', nodeSelector: 'jenkins=slave',
    yaml:"""
apiVersion: v1
kind: Pod
spec:
  tolerations:
  - key: jenkins
    operator: Equal
    value: slave
    effect: NoSchedule
    """,
    envVars: [
        envVar(key: 'AWS_DEFAULT_REGION', value: AWS_DEFAULT_REGION),
        envVar(key: 'DOCKER_BUILDKIT', value: '1')
    ],
    containers: [
        containerTemplate(name: 'go', image: 'golang:1.12-alpine', ttyEnabled: true, command: 'cat'),
        containerTemplate(name: 'docker', image: 'docker:19.03.1-dind', ttyEnabled: true, command: 'cat'),
        containerTemplate(name: 'eks-helmsman', image: 'widerin/eks-helmsman:0.3', ttyEnabled: true, command: 'cat'),
    ],
    volumes: [
        hostPathVolume(mountPath: '/var/run/docker.sock', hostPath: '/var/run/docker.sock'),
    ]
) {
    gitlabCommitStatus {
        node(POD_LABEL) {
            try {
                stage('Checkout') {
                    checkout scm
                }
                stage('Validate') {
                    VersionValidate()
                }
                stage('Test') {
                    container('docker') {
                        sh "docker build -f Dockerfile-test ."
                    }
                }
                if (env.gitlabActionType == 'MERGE' || env.gitlabActionType == 'NOTE') {
                    stage('TestBuild') {
                        container('docker') {
                            sh "docker build ."
                        }
                    }
                }
                // Start Push Action
                if(env.gitlabActionType == 'PUSH') {
                    stage('Docker Image') {
                        DockerBuildPush()
                    }
                    stage('Rollout') {
                        EksHelmDeploy()
                    }
                }
            } catch (e) {
              currentBuild.result = "FAILED"
//              NotifyFailed()
              throw e
            }
        }
    }
}
